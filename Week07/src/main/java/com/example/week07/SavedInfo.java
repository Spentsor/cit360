package com.example.week07;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Servlet implementation class SavedInfo
 */
@WebServlet(name = "SavedInfo", urlPatterns = {"/SavedInfo"})
public class SavedInfo extends HttpServlet {
    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public SavedInfo() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        PrintWriter out = response.getWriter();

        try
        {
            String firstName = request.getParameter("firstName");
            String lastName = request.getParameter("lastName");
            String phone = request.getParameter("phone");
            String email = request.getParameter("email");
            String date = request.getParameter("date");
            String add = request.getParameter("add");

            out.println("First Name :"+firstName);
            out.println("Last Name :"+lastName);
            out.println("Phone Number :"+phone);
            out.println("Email :"+email);
            out.println("Date :"+date);
            out.println("Address :"+add);


        }
        catch(Exception e)
        {
            e.toString();
        }
    }

}
