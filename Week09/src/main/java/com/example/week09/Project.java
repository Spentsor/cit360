package com.example.week09;

import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name="projectlist")
public class Project {
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "idProjectList")
    private int id;

    @Column(name = "projectName")
    private String project;

    @Column(name = "duration")
    private String duration;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getProject() {
        return project;
    }

    public void setProject(String project) {
        this.project = project;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String toString() {
        return id + " " + project + " " + duration;
    }
}
