package com.example.week09;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import java.util.*;

public class TestProject {
    SessionFactory factory = null;
    Session session = null;

    private static TestProject single_instance = null;

    private TestProject()
    {
        factory = ProjectUtilities.getSessionFactory();
    }

    /** This is what makes this class a singleton.  You use this
     *  class to get an instance of the class. */
    public static TestProject getInstance()
    {
        if (single_instance == null) {
            single_instance = new TestProject();
        }

        return single_instance;
    }

    /** Used to get more than one Project from database
     *  Uses the OpenSession construct rather than the
     *  getCurrentSession method so that I control the
     *  session.  Need to close the session myself in finally.*/
    public List<Project> getProject() {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from com.example.week09.Project";
            List<Project> cs = (List<Project>)session.createQuery(sql).getResultList();
            session.getTransaction().commit();
            return cs;

        } catch (Exception e) {
            e.printStackTrace();
            // Rollback in case of an error occurred.
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }

    }

    /** Used to get a single Project from database */
    public Project getProject(int id) {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from com.example.week09.Project where id = " + id;
            Project c = (Project)session.createQuery(sql).getSingleResult();
            session.getTransaction().commit();
            return c;

        } catch (Exception e) {
            e.printStackTrace();
            // Rollback in case of an error occurred.
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }

}
