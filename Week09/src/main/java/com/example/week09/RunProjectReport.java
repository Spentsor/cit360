package com.example.week09;


import java.util.List;

public class RunProjectReport {

    public static void main(String[] args) {
        TestProject t = TestProject.getInstance();

        List<Project> p = t.getProject();
        for(Project i: p) {
            System.out.println(i);
        }

        System.out.println(t.getProject(1));
    }
}
