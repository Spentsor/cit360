package week05;

public class TravelPoints {

    private String firstName;
    private String lastName;
    private double points;
    private int accountNumber;

    public static final int CREDITCARD = 1;
    public static final int BONUSPOINTS = 2;

    private int accountType;

    public TravelPoints(String firstName, String lastName, int points, int accountNumber, int typeOfAccount) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.points = points;
        this.accountNumber = accountNumber;
        this.accountType = typeOfAccount;
    }

    public double addPoints(int amount, boolean creditCard) {
        points += amount;
        return points;
    }

        public double spendPoints ( double amount, boolean flights){
            if ((amount > 500) & !flights) {
                throw new IllegalArgumentException();
            }

            points -= amount;
            return points;
        }

        public double getPoints () {
            return points;
        }

        public String getFirstName () {
            return firstName;
        }

        public String getLastName () {
            return lastName;
        }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAccountNumber () {
            return accountNumber;
        }

    public boolean isCreditCard() {
        return accountType == CREDITCARD;
    }
}
