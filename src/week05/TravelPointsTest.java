package week05;

import static org.junit.Assert.*;

public class TravelPointsTest {

    private TravelPoints myAccount;
    private static int count;

    @org.junit.BeforeClass
    public static void beforeClass() {
        System.out.println("Counting the test cases" + count++);
    }

    @org.junit.Before
    public void setUp() {
        myAccount = new TravelPoints("Spencer", "Griffin", 20000, 8675309, TravelPoints.CREDITCARD);
        System.out.println("Testing, I promise!!!");
    }


    @org.junit.Test
    public void addPoints() throws Exception {
        double points = myAccount.addPoints(1500, true);
        assertEquals(21500.00, points, 0);
    }

    @org.junit.Test
    public void spendPoints_creditCardRewards() throws Exception {
        double points = myAccount.spendPoints(400, true);
        assertNotEquals(19500, points, 0 );
    }

    @org.junit.Test(expected = IllegalArgumentException.class)
    public void spendPoints_creditCardFlights() throws Exception {
        double points = myAccount.spendPoints(600, false);
        assertEquals(19500, points, 0 );
    }

    @org.junit.Test
    public void getPoints() throws Exception {
        double points = myAccount.spendPoints(20000, true);
        assertNotSame(0, true);
    }

    @org.junit.Test
    public void getFirstName() throws Exception {
       assertNotNull(myAccount.getFirstName());
    }

    @org.junit.Test
    public void getLastName() throws Exception {
        myAccount.setLastName(null);
        assertNull(myAccount.getLastName());
    }

    @org.junit.Test
    public void getAccountNumber() throws Exception {
        assertNotNull(myAccount.getAccountNumber());
    }

    @org.junit.After
    public void tearDown() throws Exception {
        System.out.println("The test count is " + count++);
    }

}