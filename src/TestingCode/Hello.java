package TestingCode;

public class Hello {
    public static void main(String[] args) {

        int myValue = 10_000;
        int myMinIntValue = Integer.MIN_VALUE;
        int myMaxIntValue = Integer.MAX_VALUE;

        System.out.println("Integer Minimum Value= " + myMinIntValue);
        System.out.println("Integer Maximum Value= " + myMaxIntValue);
    }
}
