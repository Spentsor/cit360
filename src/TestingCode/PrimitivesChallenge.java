package TestingCode;

import java.util.*;
import java.util.Scanner;

public class PrimitivesChallenge {

    public static void main(String[] args) {
        double valueOne = 20.00;
        double valueTwo = 80.00;
        double result = (valueOne + valueTwo) * 100.00d;
        System.out.println(result);
        double theRemainder = result % 40;
        System.out.println(theRemainder);
        boolean isTrue = (theRemainder == 0) ? true : false;
        System.out.println(isTrue);

        if (isTrue != true) {
            System.out.println("Got some remainder");
        }
    }
}
