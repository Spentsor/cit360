package Week08;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class JustDoIt {
    public static void main(String[] args) {
        ExecutorService enoughAlready = Executors.newFixedThreadPool(5);
        JustEnough llc1 = new JustEnough("Exxon", 1000, 2000);
        JustEnough llc2 = new JustEnough("Amazon", 950, 1500);
        JustEnough llc3 = new JustEnough("Microsoft", 900, 1000);
        JustEnough llc4 = new JustEnough("Canoo", 80, 950);
        JustEnough llc5 = new JustEnough("Jersey Mikes", 800, 900);
        JustEnough llc6 = new JustEnough("Baja Jacks Burrito Shack", 750, 850);
        JustEnough llc7 = new JustEnough("Gast Inc.", 700, 800);
        JustEnough llc8 = new JustEnough("DXC", 600, 750);

        enoughAlready.execute(llc1);
        enoughAlready.execute(llc2);
        enoughAlready.execute(llc3);
        enoughAlready.execute(llc4);
        enoughAlready.execute(llc5);
        enoughAlready.execute(llc6);
        enoughAlready.execute(llc7);
        enoughAlready.execute(llc8);

        enoughAlready.shutdown();
    }
}
