package Week08;

import java.util.Random;

public class JustEnough implements Runnable {

    private String company;
    private int pay;
    private int paid;
    private int rand;


    public JustEnough(String company, int pay, int luck) {

        this.company = company;
        this.pay = pay;
        this.paid = luck;

        Random random = new Random();
        this.rand = random.nextInt(2000);
    }

    public void run() {
        System.out.println(new StringBuilder().append("Let's see if it's your lucky day. The company ").append(company).append(" is paying you ").append(pay).append(" for ").append(paid).append(" days now!").toString());
        for (int i = 1; i < rand; i++) {
            if (i % pay == 0) {
                System.out.println(company + " paid you!" );
                try {
                    Thread.sleep(paid);
                } catch (InterruptedException e) {
                    System.err.println(e.toString());
                }
            }
        }
        System.out.println(company + " has paid you enough.");
    }
}
