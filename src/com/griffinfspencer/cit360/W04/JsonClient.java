package com.griffinfspencer.cit360.W04;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import java.net.*;
import java.io.*;
import java.util.*;

public class JsonClient {

    public static String familyToJson(Family family) {
        ObjectMapper mapper = new ObjectMapper();
        String g = "";

        try {
            g = mapper.writeValueAsString(family);
        } catch (JsonProcessingException e) {
            System.err.println(e.toString());
        }
        return g;
    }
    public static Family JsonToFamily(String g) {
        ObjectMapper mapper = new ObjectMapper();
        Family family = null;

        try {
            family = mapper.readValue(g, Family.class);
        } catch (JsonProcessingException e) {
            System.err.println(e.toString());
        }
        return family;
    }

    public static String getMyInfo(String family) {


        try {
            URL url = new URL(family);
            HttpURLConnection http = (HttpURLConnection) url.openConnection();

            BufferedReader reader = new BufferedReader(new InputStreamReader(http.getInputStream()));
            StringBuilder stringBuilder = new StringBuilder();

            String line;
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line + "/n");
            }

            return stringBuilder.toString();


        } catch (IOException e) {
            System.err.println(e.toString());
        }
        return "That's a big whoopsy daisy!";
    }

    public static Map getHTTPHeaders(String family) {
        try {

            URL url = new URL(family);
            HttpURLConnection http = (HttpURLConnection) url.openConnection();

            return http.getHeaderFields();

        } catch (IOException e) {
            System.err.println(e.toString());
        }

        return null;
    }

    public static void main(String[] args) {

        System.out.println(JsonClient.getMyInfo("https://jsonplaceholder.typicode.com/users"));

        Map<Integer, List<String>> familyMap = JsonClient.getHTTPHeaders("https://jsonplaceholder.typicode.com/users");

        assert familyMap != null;
        for (Map.Entry<Integer, List<String>> entry : familyMap.entrySet() ) {

            System.out.println("Key= " + entry.getKey() + " Value= " + entry.getValue());
        }

        Family sibling = new Family();
        sibling.setName("Billy");
        sibling.setEmailAddress("orsongriffin@fakemail.com");

        String json = JsonClient.familyToJson(sibling);
        System.out.println(json);

        Family brother2 = JsonClient.JsonToFamily(json);
        System.out.println(brother2);
    }
}
